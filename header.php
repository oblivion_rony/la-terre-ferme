<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package La_Terre_Ferme
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link href="https://fonts.googleapis.com/css?family=Quicksand:500&display=swap" rel="stylesheet"> 
	<script src="https://kit.fontawesome.com/074a9e6cfb.js" crossorigin="anonymous"></script>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'la-terre-ferme' ); ?></a>	
</div>

	<header id="masthead" class="site-header">
	<div class="panier">
	<!--EMPLACEMENT POUR LE MENU PANIER-->
<?php 
	//	wp_nav_menu ( array (
	//	'theme_location' => 'panier-menu' ,
	//	'menu_class' => 'mon-panier', 
	//	) ); ?> 
	</div>

			<?php
				the_custom_logo();
			?> 

	<!-- .site-branding-->
		<nav id="site-navigation" class="main-navigation">
                <div id="head-mobile"></div>
                <div class="burger"></div>
                    <?php
                    wp_nav_menu( array(
                        'theme_location' => 'menu-1',
                        'menu_id'        => 'primary-menu',
                        'container'      => '',
                    ) );
                    ?>                   
        </nav>
							
	</header><!-- #masthead -->

	<div id="content" class="site-content"></div>
			
