<?php
/**
* Template Name:Recettes
*
* @subpackage La_Terre_Ferme
* @since La_Terre_Ferme

 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package La_Terre_Ferme
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		
		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', 'page' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>
	
            <?php 
            query_posts(array( 
                'post_type' => 'recettes',
                'showposts' => 10 
            ) );  
            ?>
            <?php while (have_posts()) : the_post(); ?>
            <div class="miniature" >
                    <div class="recette">
                        <?php the_post_thumbnail(); ?>
                        <div class="text">
                            <h2><b><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></b></h2>
                            <p><i>Recette proposée par : <?php the_author(); ?></i></p>
                            <p><?php echo get_the_excerpt(); ?></p>
                        </div>
                    </div>
            </div>
            <?php endwhile;?>
                
		
		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
