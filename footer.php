<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package La_Terre_Ferme
 */
?>
	<div class="push"></div>
	<footer id="colophon" class="site-footer">
		<!--Footer en 4 colonnes -->
		<div class="contact">
			<div class="footer_logo"><?php (dynamic_sidebar('footer_gauche'))?></div>
			<div class="centr1"><?php (dynamic_sidebar('footer_centrale'))?></div>
			<div class="centr2"><?php (dynamic_sidebar('footer_centrale2'))?></div>
			<div class="colonne_droite"><?php (dynamic_sidebar('footer_droite'))?></div>
		</div>	
		<!--copyright-->
		<div class="copy"><p class="copyp">&copy; 2020 La Terre Ferme | MRM</p></div>	
	</footer>
 <?php wp_footer(); ?>
</body>
</html>
