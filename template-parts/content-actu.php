<?php
/**
 * 
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package La_Terre_Ferme
 */

?>


<div class="postview">
	<header class="entry-header">
		
		<div class="date-header"><?php la_terre_ferme_posted_on();?></div>
		<?php
		if ( is_singular() ) :
			the_title( '<h1 class="titre">', '</h1>' );
		else :
			the_title( '<h2 class="titre"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;

		 ?>
		
		<hr>
</div></header><!-- .entry-header -->
	
<div class=flex>

<article id="post-<?php the_ID(); ?>"<?php post_class(); ?>>

	<div class="entry-actu">
		<?php
		the_content( sprintf(
			wp_kses(
				/* translators: %s: Name of current post. Only visible to screen readers */
				__( 'Lire la suite ...<span class="screen-reader-text"> "%s"</span>', 'la-terre-ferme' ),
				array(
					'span' => array(
						'class' => array(),
					),
				)
			),
			get_the_title()
		) );

		wp_link_pages( array(
			'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'la-terre-ferme' ),
			'after'  => '</div>',
		) );
		?>
	<div class="views"><?php echo getPostViews(get_the_ID());?></div>
	</div><!-- .entry-content -->
	<hr>
	</div>
	
</article><!-- #post-<?php the_ID(); ?> -->
