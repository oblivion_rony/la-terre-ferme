<?php
/**
 * La Terre Ferme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package La_Terre_Ferme
 */

if ( ! function_exists( 'la_terre_ferme_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function la_terre_ferme_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on La Terre Ferme, use a find and replace
		 * to change 'la-terre-ferme' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'la-terre-ferme', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
        add_theme_support( 'post-thumbnails' );
        // Définir la taille des images mises en avant
        set_post_thumbnail_size();

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'la-terre-ferme' ),
		) );

		

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'la_terre_ferme_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 150,
			'width'       => 150,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'la_terre_ferme_setup' );


/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function la_terre_ferme_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'la_terre_ferme_content_width', 640 );
}
add_action( 'after_setup_theme', 'la_terre_ferme_content_width', 0 );



/**
 * Enqueue scripts and styles.
 */
function la_terre_ferme_scripts() {
	wp_enqueue_style( 'la-terre-ferme-style', get_stylesheet_uri() );

	//wp_enqueue_script( 'la-terre-ferme-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
    wp_enqueue_script('custom-script', get_stylesheet_directory_uri() . '/js/nav-custom.js', array( 'jquery' ));
}
add_action( 'wp_enqueue_scripts', 'la_terre_ferme_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

// Notre fonction Article Personnalisé (Custom Post)
function create_post_type() {
  
    register_post_type( 'recettes',
        array(
            'labels' => array(
                'name' => __( 'Recettes' ),
                'singular_name' => __( 'Recette' ),
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'recettes'),
        )
    );
}
add_action( 'init', 'create_post_type' );

//------------------------------------- RECETTES----------------------------------------//
/*
* Cette fonction va créer l'apparence de notre nouveau Custom Post,
* visible ensuite dans l'administration WordPress
*/
 
function custom_post_type() {
 
    $labels = array(
        'name'                => _x( 'Recettes', 'Post Type General Name', 'ltf' ),
        'singular_name'       => _x( 'Recette', 'Post Type Singular Name', 'ltf' ),
        'menu_name'           => __( 'Recettes', 'ltf' ),
        'parent_item_colon'   => __( 'Recette Parente', 'ltf' ),
        'all_items'           => __( 'Toutes les Recettes', 'ltf' ),
        'view_item'           => __( 'Voir la Recette', 'ltf' ),
        'add_new_item'        => __( 'Ajouter une Nouvelle Recette', 'ltf' ),
        'add_new'             => __( 'Ajouter', 'ltf' ),
        'edit_item'           => __( 'Éditer une Recette', 'ltf' ),
        'update_item'         => __( 'Mettre à jour une Recette', 'ltf' ),
        'search_items'        => __( 'Rechercher une Recette', 'ltf' ),
        'not_found'           => __( 'Aucune Recette', 'ltf' ),
        'not_found_in_trash'  => __( 'Aucune Recette dans la corbeille', 'ltf' ),
    );
     
// Paramètre d'autres options :
     
    $args = array(
        'label'               => __( 'recettes', 'ltf' ),
        'description'         => __( 'Les meilleures recettes des cuisines du Monde', 'ltf' ),
        'labels'              => $labels,
 
        // Ajoute les options suivantes à l'édition des articles Recettes
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
 
        // Vous pouvez associer le Type d'Article avec un taxonomie ou une taxonomie personnalisée.  
        'taxonomies'          => array( 'cuisine' ),
 
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'menu_icon'           => 'dashicons-carrot', //icône associé au menu, ici une Carotte pour illustrer les recettes (plus d'infos dans la suite du tuto)
        'can_export'          => true, 
        'has_archive'         => true, 
        'exclude_from_search' => false, 
        'publicly_queryable' => true, 
        'capability_type'    => 'post', ); 
 
       // On enregistre notre Type d'Article Personnalisé (Custom Post Type) 
       register_post_type( 'recettes', $args ); } 
 
add_action( 'init', 'custom_post_type', 0 );


//----------------------------- Mise en place widgets pour le footer ----------------------------------------//

function register_widget_areas () {
    // Colonne gauche
    register_sidebar (array (
        'name' => ('Footer Colonne gauche'),
        'id' => 'footer_gauche',
        'before_widget' => '<div class="footer-widget">',
        'after_widget' => '</div>',
    ));
    
    // Colonne centrale gauche
    register_sidebar (array (
        'name' => ('Footer Colonne centrale Gauche'),
        'id' => 'footer_centrale',
        'before_widget' => '<div class="footer-widget">',
        'after_widget' => '</div>',
    ));
    
    // Colonne centrale droite
    register_sidebar (array (
        'name' => ('Footer Colonne centrale Droite'),
        'id' => 'footer_centrale2',
        'before_widget' => '<div class="footer-widget">',
        'after_widget' => '</div>',
    ));
    
    // Colonne droite
    register_sidebar (array (
        'name' => ('Footer Colonne droite'),
        'id' => 'footer_droite',
        'before_widget' => '<div class="footer-widget">',
        'after_widget' => '</div>',
    ));
    
    }
    
    add_action ('widgets_init', 'register_widget_areas');




  // Modification logo page admin Wordpress__________________________________________

  function wpmylogo(){
    ?>
        <style>
            body.login div#login h1 a{
                background-image:url(https://laterreferme.yj.fr/wp-content/uploads/2020/03/LogoDef.png)
            }
    </style>
    <?php
    }
    add_action('login_enqueue_scripts','wpmylogo');

 
/*Gestion des Vues*/
function getPostViews($postID){
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0 Vue";
    }
    return $count.' Vues';
}
function setPostViews($postID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
    $count = 0;
    delete_post_meta($postID, $count_key);
    add_post_meta($postID, $count_key, '0');
    }else{
    
    /*
    * Check the cookie is exist or not.
    * If not the set cookie for some time interval and
    * increase the count
    */
    if(!isset($_COOKIE['wpai_visited_ip']))
    
    $count++;
    update_post_meta($postID, $count_key, $count);
    
    // Get IP address
    $visit_ip_addr = $_SERVER['REMOTE_ADDR'];
    
    // Set the cookie
    setcookie('wpai_visited_ip', $visit_ip_addr, time()+ (60 * 1));
    
    }
}
// Remove issues with prefetching adding extra views
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);


// Modification nom d'un bloc dans une fiche produit WooCommerce_____________________-

add_filter( 'woocommerce_product_tabs', 'wpm_rename_tabs', 98 );
function wpm_rename_tabs( $tabs ) {

    $tabs['description']['title'] = __( 'Recette' );// Renomme le bloc "Description" en "recette"
    
    return $tabs;

}

// Gestion de WooCommerce 
function la_terre_ferme_add_woocommerce_support() {
	add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'la_terre_ferme_add_woocommerce_support' );

function disable_woo_commerce_sidebar() {
	remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10); 
}
add_action('init', 'disable_woo_commerce_sidebar');

// Sécurité
// Message erreur de co
/* Masquer les erreurs de connexion à l'administration */

add_filter('login_errors', 'wpm_hide_errors');

function wpm_hide_errors() {
	// On retourne notre propre phrase d'erreur
	return "L'identifiant ou le mot de passe est incorrect";
}

// Suppr version WP
remove_action("wp_head", "wp_generator");
